Feature: Basic Navigation flow

    Top, doc and page navigation should work

    Scenario: Open page with TOP navigation
        Given I am on "Landing" page
        When I Open "Docs" page from TOP navigation
        Then TOP navigation active item is "Docs"

    Scenario: Open document with DOC navigation
        When I Open "Automation Protocols" from DOC navigation
        Then DOC navigation active item is "Automation Protocols"
        And  DOC page header is "Automation Protocols"

    Scenario: Scroll to anchor on page
        When I Open "DevTools Protocol" from PAGE navigation
        Then Window url contains "devtools-protocol"
        And  DOC anchor "devtools-protocol" with text "DevTools Protocol" is visible
