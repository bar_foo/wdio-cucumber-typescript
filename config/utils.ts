export const setSeleniumArgs = (config: WebdriverIO.Config, drivers: { [key: string]: string }) => {
    const seleniumOpts = (config.services?.find(
        (service) => Array.isArray(service) && service[0] === 'selenium-standalone'
    ) as [string, WebdriverIO.ServiceOption])[1]

    seleniumOpts.drivers = { ...drivers, ie: false }
}
