import { Given } from 'cucumber'

Given(/^I am on "([^"]*)" page$/, (page: string) => {
    switch (page) {
        case 'Landing':
            browser.url('/')
            break
        default:
            throw new Error(`Unable to open page "${page}". Not implemented.`)
    }
})
