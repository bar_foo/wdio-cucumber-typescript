import { Then } from 'cucumber'

import { topNavigation } from '../elements/topNav'
import { documentNavigation } from '../elements/docNav'
import { docPage } from '../pages/docPage'

Then(/^Window title is "([^"]*)"$/, (title: string) => {
    expect(browser).toHaveTitle(title)
})

Then(/^Window url contains "([^"]*)"$/, (str: string) => {
    expect(browser).toHaveUrl(str, { containing: true })
})

Then(/^(\w+) navigation active item is "([^"]*)"$/, (navType: string, link: string) => {
    let navigation
    switch (navType) {
        case 'TOP':
            navigation = topNavigation
            break
        case 'DOC':
            navigation = documentNavigation
            break
        default:
            throw new Error(`Unsupported navigation type: "${navType}"`)
    }

    expect(navigation.$getActive()).toHaveText(link)
})

Then(/^DOC page header is "([^"]*)"$/, (header: string) => {
    expect(docPage.$getPostHeader()).toHaveText(header)
})

Then(/^DOC anchor "([^"]*)" with text "([^"]*)" is visible$/, (anchor: string, text: string) => {
    const subHeader = docPage.getAnchorHeaderById(anchor)
    expect(subHeader).toHaveText(text)
    expect(subHeader).toBeDisplayedInViewport()
})
