class DocPage {
    get $origin() {
        return $('.container.mainContainer')
    }

    public $getPostHeader() {
        return this.$origin.$('.postHeaderTitle')
    }

    public getAnchorHeaderById(id: string) {
        return this.$origin.$(`.//a[@class="anchor" and @id="${id}"]/..`)
    }
}
export const docPage = new DocPage()
